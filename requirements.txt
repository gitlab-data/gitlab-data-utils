snowflake-sqlalchemy>=1.1.10, <2.0
snowflake-connector-python[pandas]>=2.7.8
pandas>=0.25.3
pyyaml>=6.0.0, <=6.0.2
pygsheets>=2.0.5
